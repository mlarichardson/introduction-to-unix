#! /bin/bash

# Bash introduction:  # ... is a comment
# You can make scripts be verbose by adding "set -v" (which just writes out each line as it gets run)
#   or "set -x" (which prints out each line with variables expanded and does formatting that's neater than -v
# set -v
# set -x

# Note - indenting DOESN'T MATTER

echo "Hello World!"
echo "Number of arguments is " $#
echo "First argument is " $1

# Basic variables
echo "Present working directory is : " $PWD
echo "pwd returns: "
pwd

echo "The current shell is in format " $SHELL

# Basic commands: ">>" means things you enter at the command prompt.
#  Required arguments will be inside {}, optional arguments will be inside []
#  Some have options by adding arguments of the form -X where X = lower or upper case letter. 
#  To see valid options or discussion of a command do ">> man COMMAND"; quit with "q":


# >> man {command1} = manual for a given command
#     NB : enter q to quit
# >> pwd = Print Working Directory
# >> mkdir {1} = MaKe DIRectory {DIRECTORY NAME}
# >> cd {1} = Change Directory {DIRECTORY NAME}
# NB: "." or "./" defaults to current directory
# NB: "../" is a alias for "Up one directory" : ie ">> cd ../" would go up one directory
# >> ls [1] [2] ... = LiSt contents/details of directories/files [DIRECTORY1] [DIRECTORY2] [FILE1] ... 
#                      with no arguments it defaults to listing the contents of the current directory.
# cp/mv {1} [2] [3] [4] ... [] {N} = CoPy or MoVe file(s) {FILE1} [FILE2] [FILE3] ... to location {DIRECTORY}
#                                 NB: If more than one file is specified to be copied, the final argument must be a directory
#                                     Otherwise a possibly new file is generated which is a copy if cp is used of the first argument
# touch {1} [2] [3] ... = create file(s) {1} [2] ... or update the last time they were modified if they already exist.

pwd                           # This announces your current directory
mkdir test                    # This makes a directory called test  
cd test                       # This changes directory to the new directory you made, call test
pwd                           # This announces your current directory which now is test
ls                            # this lists all of the files in this directory which is nothing
touch test_file.txt           # this creates and empty file called test_file.txt
ls                            # Now you see the file
mv test_file.txt new_file.txt # This changes the name of the file. You could have specified a different directory, which would move the file there too
ls
cp new_file.txt test_file.txt
ls
ls -l # Now you see more details - size = 0 and created just now. 
cd ../
pwd
ls
ls -R # Lets you recursively list files in all directories down the local directory tree.
ls \
-R # \ is an escape character, so it escapes the "Return Line", acting as a continuation character


# Still to cover:
#   grep (GRab Entered Phrase?): Need to do an aside on regular expression: http://www.grymoire.com/Unix/Regular.html
#   sed (Stream EDitor) (see regular expression) : http://www.grymoire.com/Unix/Sed.html
#   awk 