#! /bin/bash

if [[ $# -lt 1 ]] ; then
  echo "Error: Usage MakeSAve.sh NUM"
  exit 1
fi

NUM=$1
head -2 Prof_R_S_Wide_$NUM.txt | sed 's/,/ave,/' | sed 's/radius *$/radiusave/' > Prof_R_Save_Wide_$NUM.txt
tail -64 Prof_R_Rho_Wide_$NUM.txt | sed 's/,/ /' | awk '{print (0.76*$2/1.66e-24)^0.6666666}' > tmp_rho23 
tail -64 Prof_R_T_Wide_$NUM.txt | sed 's/,/ /' > tmp_t
paste tmp_t tmp_rho23 | awk '{print $1","8.617e-8*$2/$3}' >> Prof_R_SAve_Wide_$NUM.txt
rm tmp_*
